<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cookies Consent</title>
    <link href="dist/cookie-consent.css?v=232423432" rel="stylesheet" />
</head>
<body>
    <h1>Cookies Consent Demo</h1>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132550778-6"></script>
    <script src="dist/cookieproof.umd.js"></script>
    <script>
        const cookies = new cookieproof.CookieProof({
            content: {
                href: 'https://google.com'
            }
        });

        // // Set our cookies
        cookies.addCookie({
            add: function() {
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', 'UA-132550778-6');
            },
            remove: function() {                
                cookies.deleteCookie('_ga');
                cookies.deleteCookie('_gid');
                cookies.deleteCookie('_gat');
                cookies.deleteCookie('AMP_TOKEN');
                window['ga-disable-UA-132550778-6'] = true;
                cookies.deleteCookie('_gac_UA-132550778-6');
                cookies.deleteCookie('_gat_gtag_UA_132550778_6');
            }
        });

        // // Ask for consent.
        cookies.askForConsent();
    </script>

</body>
</html>
