import './cookie-consent';
declare global {
    interface Window {
        cookieconsent: any;
    }
}
/**
 * CookieProof is a cookie consent script based on the Cookie Consent library by Osano.
 *
 * @exports
 * @class CookieProof
 */
export declare class CookieProof {
    /**
     * Contains the cookies to enable/disable
     */
    private cookies;
    private options;
    /**
     * Sets our options
     *
     * @param options The provided options
     */
    constructor(options: CookieProofOptions);
    /**
     * Displays the cookies request message
     *
     * @public
     */
    askForConsent(): void;
    /**
     * Adds a cookie to our list
     *
     * @param cookie The cookie to add
     */
    addCookie(cookie: CookieProofCookie): void;
    /**
     * Helper method to delete the cookie
     *
     * @param name The name of the cookie
     */
    deleteCookie(name: string): void;
    /**
     * Loops through our cookie objects and enables them
     *
     * @private
     */
    private allow;
    /**
     * Loops through our cookie objects and disables them
     *
     * @private
     */
    private deny;
    /**
     * Combines any objects passed into it. For deep extending, set the first parameter to true
     *
     * @param args The objects to extend
     */
    private static extend;
    /**
     * Returns the current domain name without the subdomain
     */
    private getDomainWithoutSubdomain;
}
