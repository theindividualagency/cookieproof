interface CookieProofOptions {

    // Colour Options
    palette?: {
        popup?: {
            background: string;
        },
        button?: {
            background?: string;
            text?: string;
            border?: string
        }
    },

    // Content options
    content?: {
        message?: string;
        dismiss?: string;
        allow?: string;
        href?: string;
    }

    // Options
    theme?: string;
    type?: string,
    position?: string;
    revokable?: boolean;
    animateRevokable?: boolean;

    // Callbacks
    onInitialise?: (status: string) => void,
    onStatusChange?: (status: string) => void,
    onRevokeChoice?: () => void
}

interface CookieProofCookie {
    add: () => void,
    remove: () => void
}
