// @ts-ignore
import './cookie-consent';

declare global {
    interface Window { 
        cookieconsent: any;
    }
}

/**
 * CookieProof is a cookie consent script based on the Cookie Consent library by Osano.
 * 
 * @exports
 * @class CookieProof
 */
export class CookieProof {


    /**
     * Contains the cookies to enable/disable
     */
    private cookies: CookieProofCookie[] = [];


    // Set our default options
    private options: CookieProofOptions = {
        palette: {
            popup: {
                background: '#000'
            },
            button: {
                background: "transparent",
                text: "#f1d600",
                border: "#f1d600"
            },
        },
        type: "opt-in",
        revokable: false,
        content: {
            href: '/cookie-policy/',
            message: "We would like to use analytics cookies to give you the best experience on this website.",
            allow: "Allow",
            dismiss: "Dismiss"
        },
        onInitialise: (status) => {
            status === 'allow' ? this.allow() : this.deny();
        },
        onStatusChange: (status) => {
            status === 'allow' ? location.reload() : this.deny();
        }
    };


    /**
     * Sets our options
     *
     * @param options The provided options
     */
    constructor(options: CookieProofOptions) {
        this.options = CookieProof.extend(true, this.options, options);
    }


    /**
     * Displays the cookies request message
     * 
     * @public
     */
    public askForConsent() {
        window.cookieconsent.initialise(this.options);
    }


    /**
     * Adds a cookie to our list
     *
     * @param cookie The cookie to add
     */
    public addCookie(cookie: CookieProofCookie) {
        this.cookies.push(cookie);
    }
    

    /**
     * Helper method to delete the cookie
     * 
     * @param name The name of the cookie
     */
    public deleteCookie(name: string) {
        document.cookie = name + '=; Path=/; Domain=.' + this.getDomainWithoutSubdomain() + '; Expires=Thu, 01 Jan 1970 00:00:01 GMT;'
    }


    /**
     * Loops through our cookie objects and enables them
     * 
     * @private
     */
    private allow() {
        this.cookies.forEach(cookie => {
            cookie.add();
        });
    }


    /**
     * Loops through our cookie objects and disables them
     * 
     * @private
     */
    private deny() {
        this.cookies.forEach(cookie => {
            cookie.remove();
        });
    }


    /**
     * Combines any objects passed into it. For deep extending, set the first parameter to true
     *
     * @param args The objects to extend
     */
    private static extend(...args: any) {

        // Variables
        var extended: any = {};
        var deep = false;
        var i = 0;
        var length = args.length;
    
        // Check if a deep merge
        if ( Object.prototype.toString.call( args[0] ) === '[object Boolean]' ) {
            deep = args[0];
            i++;
        }
    
        // Merge the object into the extended object
        var merge = function (obj: any) {
            for ( var prop in obj ) {
                if ( Object.prototype.hasOwnProperty.call( obj, prop ) ) {

                    // If deep merge and property is an object, merge properties
                    if ( deep && Object.prototype.toString.call(obj[prop]) === '[object Object]' ) {
                        extended[prop] = CookieProof.extend( true, extended[prop], obj[prop] );
                    } else {
                        extended[prop] = obj[prop];
                    }
                }
            }
        };
    
        // Loop through each object and conduct a merge
        for ( ; i < length; i++ ) {
            var obj = args[i];
            merge(obj);
        }
    
        return extended;
    };


    /**
     * Returns the current domain name without the subdomain
     */
    private getDomainWithoutSubdomain() {
        const parts     = location.hostname.split('.');
        if(parts.length > 2) parts.shift();
        return parts.join('.');
    }
}
